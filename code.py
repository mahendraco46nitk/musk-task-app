import urllib.request
from bs4 import BeautifulSoup
import csv
import dropbox

output_file = open('output.txt','w')
output_file.write('TIN\n')
output_file.write('CST Number\n')
output_file.write('Dealer Name\n')
output_file.write('Dealer Address \n')
output_file.write('State Name\n')
output_file.write('PAN\n')
output_file.write('Date of Registration under CST Act(DD/MM/YY)\n')
output_file.write('Dealer Registration Status under CST Act\n')
output_file.write('This record is valid as on(DD/MM/YY)\n\n')

with open('input.txt','r') as input_file:
	tins = input_file.readlines()
	for tin in tins:
		url = 'http://www.tinxsys.com/TinxsysInternetWeb/dealerControllerServlet?tinNumber='+tin+'&searchBy=TIN&backPage=searchByTin_Inter.jsp'
		#print(url)
		page = urllib.request.urlopen(url).read()
		soup = BeautifulSoup(page,'html.parser')
		soup.prettify()
		for td in soup.findAll('td',{'class':'tdWhite'}):
			RString = td.get_text()
			FString = ' '.join(RString.split())
			Condition = 'Dealer Not Found for the entered TIN '+tin
			#print(FString)
			if Condition in FString:
				output_file.write(Condition)
				output_file.write('\n')
			else:
				output_file.write(FString)
				output_file.write('\n')
		output_file.write('\n')
output_file.close()
input_file.close()

client = dropbox.client.DropboxClient('oJxSpDC6AnAAAAAAAAAAH3dRyRoydFVGzh3Y4VMiz-vzYbNtiIdTgRipMFM9ZVC4')
f = open('output.txt', 'rb')
response = client.put_file('/musk_output.txt', f)
f.close()
